import React, { useEffect } from 'react'
import TeamItem from './TeamItem'
import Aos from "aos";
import 'aos/dist/aos.css'

const Teams = () => {
    useEffect(() => {
        Aos.init({ duration: 2000 })
    })
    return (
        <div className="teams pt-5" style={{ paddingBottom: '150px' }}>
            <div className="container-fluid">
                <h1 className="text-center mb-5">TEAM</h1>
                <div data-aos="fade-up" className="row justify-content-center my-5">
                    <TeamItem cl="col-lg-2 col-8 mt-4" img="/assets/img/BigSteppa - Marketing Lead.jpg" name="BigSteppa" position="Marketing Lead" />
                </div>
                <div data-aos="fade-up" className="row justify-content-center my-5">
                    <TeamItem cl="col-lg-2 col-6 mt-4" img="/assets/img/Marz - Community Lead.jpg" name="Marz" position="Community Lead" discord="Marz#3500" />
                    <TeamItem cl="col-lg-2 col-6 mt-4" img="/assets/img/Muscle - Executive Partner & Community Lead.jpg" name="Muscle" position="Executive Partner & Community Lead" discord="The Muscle#3581" />
                    <TeamItem cl="col-lg-2 col-6 mt-4" img="/assets/img/Eitz - Project Manager.jpg" name="Eitz" position="Project Manager" discord="Eitz#0021" />
                </div>
                <div data-aos="fade-up" className="row justify-content-center my-5 bw">
                    <TeamItem cl="col-lg-2 col-6 mt-4" img="/assets/img/Soulja Boy - Executive Partner.jpg" name="Soulja Boy" position="Executive Partner" discord="Discord Hidden" />
                    <TeamItem cl="col-lg-2 col-6 mt-4" img="/assets/img/Juicy J - Executive Partner.jpg" name="Juicy J" position="Executive Partner" discord="Discord Hidden" />
                    <TeamItem cl="col-lg-2 col-6 mt-4" img="/assets/img/Boose - Executive Partner.jpg" name="Boosie" position="Executive Partner" discord="Discord Hidden" />
                    <TeamItem cl="col-lg-2 col-6 mt-4" img="/assets/img/Kyle Massey - Executive Partner.jpg" name="Kyle Massey" position="Executive Partner" discord="Discord Hidden" />
                </div>
                <div data-aos="fade-up" className="row justify-content-center my-5">
                    <TeamItem cl="col-lg-2 col-6 mt-4" img="/assets/img/Spun - Project Manager.jpg" name="Spun" position="Project Manager" discord="spun#8133" />
                    <TeamItem cl="col-lg-2 col-6 mt-4" img="/assets/img/Bull Gates - Community Manager.jpg" name="Bull Gates" position="Community Manager" discord="Bull Gates#2087" />
                    <TeamItem cl="col-lg-2 col-6 mt-4" img="/assets/img/Undead - Collab Manager.jpg" name="Undead" position="Collab Manager" discord="UndeadAlive#1787" />
                    <TeamItem cl="col-lg-2 col-6 mt-4" img="/assets/img/Alex - Social Media Manager.jpg" name="Alex" position="Social Media Manager" discord="CryptoNaturally#4655" />
                    <TeamItem cl="col-lg-2 col-6 mt-4" img="/assets/img/Filip - Community Manager.jpg" name="Filip" position="Community Manager" discord="OG_J0ker#9682" />
                </div>
            </div>
        </div>
    )
}

export default Teams
