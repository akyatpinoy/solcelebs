import React from 'react'

const TeamItem = (props) => {
    return (
        <div className={props.cl}>
            <img src={props.img} alt={props.name} className="w-100 rounded-circle" onDragStart={(e) => { e.preventDefault() }} />
            <h3 className="text-center mt-3">{props.name}</h3>
            <p className="text-center">{props.position}</p>
            <div className={props.discord ? 'text-center' : 'd-none'}>
                <span><i className="fab fa-discord"></i> {props.discord}</span>
            </div>
        </div>
    )
}

export default TeamItem
